import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:8000/api';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        data: {},
        contracts: []
    },
    getters: {

        // loggedIn(state) {
        //   return state.token !== null
    },
    mutations: {
        loadSettings(state, payload) {
            state.data = payload
        },
        loadContracts(state, payload) {
            state.contracts = payload
        },
        addContract(state, payload) {
            state.data.push({
                addr: payload.addr,
                abi: payload.abi
            })
        },
        delContract(state, payload) {
            state.data.splice(payload, 1)
        }
    },
    actions: {
        // fetchUserData(context)
	    //      this.commit('loadUserdata', response.data);
    }
})