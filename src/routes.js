import DashboardMain from './components/DashboardMain.vue'
import DashboardList from './components/DashboardList.vue'


export const routes = [
    { path: '/', component: DashboardMain},
    { path: '/list', component: DashboardList}

]