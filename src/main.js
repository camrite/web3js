import Vue from 'vue'
import store from './store.js'
import {router} from './router'
import App from './App.vue'
import axios from 'axios'
import Loading from './components/Loading.vue'

Vue.prototype.$http = axios;
Vue.component('loading', Loading);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.commit('loadSettings', localStorage.getItem('app_settings') || null)
    this.$store.commit('loadContracts', localStorage.getItem('app_contracts') || null)
  }
})
